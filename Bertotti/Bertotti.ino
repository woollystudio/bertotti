
// CHEZ BERTOTTI !
// ===============
//
// Algorithm written in order to operate the illuminated sign "Chez Bertotti",
// part of the staging of the play "Parfois j'aimerais avoir une famille comme celle de la petite maison dans la prairie"
// and commissioned by the company "Hecho en Casa".
//
// Written by Martin Saëz,
// on the 25 of november, 2020 - 23:59:15

// /!\ /!\ /!\
//
// To update the arduino board, use the ICSP headers :
// https://www.arduino.cc/en/pmwiki.php?n=Tutorial/ArduinoISP
//
// The USB com port is not available anymore since it's taken by the DMX protocol.
//
// /!\ /!\ /!\

// Libraries
// ========================================================================================================================

#include <Wire.h>

#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

#include <Adafruit_PWMServoDriver.h>

#include <DMXSerial.h>

// Constant variables
// ========================================================================================================================

#define DMX_RES 256                 // DMX maximum level resolution
#define PWM_RES 4096                // PWM maximum level resolution
#define HOW_MANY_GROUPS_OF_LEDS 13
#define DMX_MODE_A_MAX 85           // DMX-Mode ranges
#define DMX_MODE_B_MAX 170
#define DMX_MODE_C_MAX DMX_RES - 1
#define DMX_MODE_REFRESH 80         // Update the DMX-Mode every XXX ms
#define DEFAULT_LEVEL 2048          // 4096 Max-PWM-level divide by two

// Objects creation from various classes
// ========================================================================================================================

Adafruit_7segment matrix = Adafruit_7segment();

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

unsigned long time;

// Variables declaration
// ========================================================================================================================

// DMX Addr
unsigned int DMX_ADD = 1;
unsigned int DMX_MODE_ADD, DMX_MASTER_ADD, DMX_MAN_ADD[HOW_MANY_GROUPS_OF_LEDS];

int LAST_MODE_UPDATE = -9999; // Random low negative number to ensure you will update the mode on the first loop

byte DMX_MODE = 0;
byte DMX_MASTER;
byte DMX_MAN[HOW_MANY_GROUPS_OF_LEDS];
//byte DMX_MAN_LAST[HOW_MANY_GROUPS_OF_LEDS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

// ========================================================================================================================

void setup ()
{
  // DMX mode setup
  DMXSerial.init(DMXReceiver, 4);

  // PWM setup
  pwm.begin();
  pwm.setOscillatorFrequency(26000000);
  pwm.setPWMFreq(1600);

  // I2C bus setup
  Wire.setClock(400000);

  // Display setup
  matrix.begin(0x70);

  // DMX address setup,
  // from the HW DIP-Switch
  for (int i = 5; i < 14; i++)
  {
    pinMode(i, INPUT_PULLUP);
    if (digitalRead(i) == LOW) DMX_ADD += pow(2, i - 5);
  }

  DMX_MODE_ADD = DMX_ADD;                           // Channel #1 is reserved for choosing the DMX-Mode (All, auto, man)
  DMX_MASTER_ADD = DMX_ADD + 1;                     // Channel #2 is reserved to the general level master

  for (int i = 0; i < HOW_MANY_GROUPS_OF_LEDS; i++) // Channels #3-15 are reserved to control each group of leds
    DMX_MAN_ADD[i] = DMX_ADD + 2 + i;               // if the DMX-Mode "man" has been choosen

  // Testing loops
  for (int i = 0; i < HOW_MANY_GROUPS_OF_LEDS; i++)
  {
    pwm.setPWM(i, 4096, 0);
    delay(333);
    pwm.setPWM(i, 0, 4096);
  }

  for (int i = HOW_MANY_GROUPS_OF_LEDS - 2; i >= 0; i--)
  {
    pwm.setPWM(i, 4096, 0);
    delay(333);
    pwm.setPWM(i, 0, 4096);
  } delay(500);

  // Print the DMX address
  // on the display to ensure
  // the setup is over
  matrix.print(DMX_ADD, DEC);
  matrix.writeDisplay();
}

// ========================================================================================================================

void loop()
{
  // Get how long ago we haven't
  // received anything from the DMX-Protocol
  unsigned long lastPacket = DMXSerial.noDataSince();

  // If DMX is available
  if (lastPacket < 5000)
  {
    time = millis(); // How long ago this sketch is running ?

    if (time - LAST_MODE_UPDATE > DMX_MODE_REFRESH)
    {
      modeUpdate();
      LAST_MODE_UPDATE = time;
    }

    switch (DMX_MODE)
    {
      case 0:
        allMode();
        break;

      case 1:
        autoMode();
        break;

      case 2:
        manMode();
        break;

      default:
        allMode();
        break;
    }
  }

  // If DMX is lost or if there is no DMX
  else
    for (int i = 0; i < HOW_MANY_GROUPS_OF_LEDS; i++)
      pwm.setPWM(i, 0, DEFAULT_LEVEL);
}

byte modeUpdate()
{
  byte CURRENT_DMX_MODE = DMXSerial.read(DMX_MODE_ADD);

  // DMX-Mode : 0-85
  if (CURRENT_DMX_MODE < DMX_MODE_A_MAX + 1
      //&& CURRENT_DMX_MODE >= 0
     ) DMX_MODE = 0; // DMX-Mode : All

  // DMX-Mode : 86-170
  else if (CURRENT_DMX_MODE < DMX_MODE_B_MAX + 1
           //&& CURRENT_DMX_MODE > DMX_MODE_A_MAX
          ) DMX_MODE = 1; // DMX-Mode : Auto

  // DMX-Mode : 170-255
  else if (CURRENT_DMX_MODE < DMX_MODE_C_MAX + 1
           //&& CURRENT_DMX_MODE > DMX_MODE_B_MAX
          ) DMX_MODE = 2; // DMX-Mode : Man

  return DMX_MODE;
}

void allMode()
{
  if (DMXSerial.dataUpdated())
  {
    DMX_MASTER = DMXSerial.read(DMX_MASTER_ADD);

    for (int i = 0; i < HOW_MANY_GROUPS_OF_LEDS; i++)
      pwm.setPWM(i, 0, DMX_MASTER * PWM_RES / DMX_RES);
  }
}

void autoMode()
{
  int flash_delay = 33;
  int short_delay = 150;
  int long_delay = 1000;

  // Set all the leds off
  for (int i = 0; i < 16; i++)
    pwm.setPWM(i, 0, PWM_RES);

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Blink as a 'wave'
  for (int i = 4; i < 9; i++)
  {
    pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay);
    pwm.setPWM(i, 0, PWM_RES);
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Blink as a 'wave'
  for (int i = 8; i > 3; i--)
  {
    pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay);
    pwm.setPWM(i, 0, PWM_RES);
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Blink as a 'wave'
  for (int i = 4; i < 9; i++)
  {
    pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay);
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;
  delay(long_delay);

  // Flashing
  for (int i = 0; i < 30; i++)
  {
    pwm.setPWM(9, 0, PWM_RES);
    delay(flash_delay);

    pwm.setPWM(9, PWM_RES, 0);
    delay(flash_delay);
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Set all the leds off
  // except the Pizza !
  for (int i = 0; i < 16; i++)
  {
    pwm.setPWM(i, 0, PWM_RES);
    if (i == 3) i = 9;
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Blink as a 'wave'
  for (int i = 0; i < 16; i++)
  {
    pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay);
    pwm.setPWM(i, 0, PWM_RES);

    if (i == 3) i = 9;
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;

  // Blink as a 'wave'
  for (int i = 0; i < 16; i++)
  {
    pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay);

    if (i == 3) i = 9;
  }

  if (DMXSerial.dataUpdated() && modeUpdate() != 1) return;
  delay(long_delay);

  // General blink
  for (int i = 0; i < 10; i++)
  {
    for (int i = 0; i < 16; i++)
      pwm.setPWM(i, PWM_RES, 0);
    delay(short_delay * 2 / 3);

    for (int i = 0; i < 16; i++)
      pwm.setPWM(i, 0, PWM_RES);
    delay(short_delay * 2 / 3);
  }
}

void manMode()
{
  if (DMXSerial.dataUpdated())
  {
    DMX_MASTER = DMXSerial.read(DMX_MASTER_ADD);

    for (int i = 0; i < HOW_MANY_GROUPS_OF_LEDS; i++)
    {
      if (DMXSerial.read(DMX_MAN_ADD[i]) == DMX_RES - 1)
        pwm.setPWM(i, 0, DMX_MASTER * PWM_RES / DMX_RES);
      else
        pwm.setPWM(i, 0, 0);
    }
  }
}

// END
// ========================================================================================================================
