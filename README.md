# Bertotti

The arduino firmware "Bertotti.ino" is available in the Bertotti subdirectory. Take care to upload the code on the board with an ICSP programmer instead of the USB com port cause this one is busy for receiving DMX datas.</br>
The "Interface.maxpat" file is a [Max](https://cycling74.com/products/max) patcher example written to control the lights with the DMX (via Artnet) protocol. This patcher uses the "IMP.DMX" externals available here : https://www.theimpersonalstereo.com/impdmx

![](./Pictures/Chez-Bertotti-ISO-FACE.png)
